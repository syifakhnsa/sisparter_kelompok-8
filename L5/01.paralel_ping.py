# import os, re dan threading
import os
import re
import threading

# import time
import time

# buat kelas ip_check
class ip_check(threading.Thread):

    # fungsi __init__; init untuk assign IP dan hasil respons = -1
    def __init__ (self,ip):
        threading.Thread.__init__(self)
        self.ip = ip
        self.status = -1
    # fungsi utama yang diekseskusi ketika thread berjalan
    def run(self):
        # lakukan ping dengan perintah ping -n (gunakan os.popen())
        ping_out = os.popen("ping -n -c2 "+ip,"r")
        # loop forever
        while True:
            # baca hasil respon setiap baris
            line = ping_out.readline()
            # break jika tidak ada line lagi
            if not line: break
            # baca hasil per line dan temukan pola Received = x
            igot = re.findall(ip_check.lifeline,line)
            if igot:
                self.status = int(igot[0])
            # tampilkan hasilnya


    # fungsi untuk mengetahui status; 0 = tidak ada respon, 1 = hidup tapi ada loss, 2 = hidup
    def status(self):
        # 0 = tidak ada respon
        if(self.__successful_pings == 0):
            return "Tidak Ada Respon"
        # 1 = ada loss
        elif(self.__successful_pings == 1):
            return "Ada Loss"
        # 2 = hidup
        elif(self.__successful_pings == 2):
            return "Hidup"
        # -1 = seharusnya tidak terjadi
        else:
            return "Seharusnya tidak Terjadi"
# buat regex untuk mengetahui isi dari r"Received = (\d)"
ip_check.lifeline = re.compile(r"received = (\d)")
# catat waktu awal
start = time.time()

# buat list untuk menampung hasil pengecekan
hasil = []

# lakukan ping untuk 20 host
for suffix in range(1,21):
    # tentukan IP host apa saja yang akan di ping
    ip = "127.0.1."+str(suffix)
    # panggil thread untuk setiap IP
    current = ip_check(ip)
    # masukkan setiap IP dalam list
    hasil.append(current)
    # jalankan thread
    current.start()

# untuk setiap IP yang ada di list
for el in hasil:
    el = hasil[0]
    hasil = hasil[1:]

    # tunggu hingga thread selesai
    el.join()

    # dapatkan hasilnya
    print("Status dari ip ",el.ip, " adalah ",report[el.status])

# catat waktu berakhir
stop = time.time()

# tampilkan selisih waktu akhir dan awal
print("Waktu yang ditempuh",stop - start)

