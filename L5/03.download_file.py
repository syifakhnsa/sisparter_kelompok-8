import os #pemanggilan fungsi sistem os
import requests #panggil library requests
import threading #panggil library threading
import urllib.request, urllib.error, urllib.parse #panggil library urllib.request, urllib.error, urllib.parse
import time #panggil library time

#variabel url berisi link untuk mendownload gambar
url = "https://apod.nasa.gov/apod/image/1901/LOmbradellaTerraFinazzi.jpg"


def buildRange(value, numsplits):#fungsi untuk mengembalikan nilai ke dalam list
    lst = []#membuat variabel array untuk menampung list
    #menambahkan ke list dan memformat suatu string
    for i in range(numsplits):#perulangan dari i sampai numsplit
        if i == 0:#jika 1=0
            #masukkan persamaan non-trivial seperti di bawah ini
            lst.append('%s-%s' % (i, int(round(1 + i * value/(numsplits*1.0) + value/(numsplits*1.0)-1, 0))))
        else:#jika i selain 0
            #masukkan persamaan non-trivial seperti di bawah ini
            lst.append('%s-%s' % (int(round(1 + i * value/(numsplits*1.0),0)), int(round(1 + i * value/(numsplits*1.0) + value/(numsplits*1.0)-1, 0))))
    return lst #mengembalikan hasil nilai

class SplitBufferThreads(threading.Thread):#fungsi untuk single thread
    """ Splits the buffer to ny number of threads
        thereby, concurrently downloading through
        ny number of threads.
    """
    def __init__(self, url, byteRange):#constructor _init_
        super(SplitBufferThreads, self).__init__()#panggil kelas parent dari constructor _init_
        self.__url = url#membuat sintaks url dan mengisi var url
        self.__byteRange = byteRange#membuat sintaks byteRange dan mengisi var byteRange
        self.req = None#membuat sintaks req dan mengosongkan var req

    def run(self):#fungsi run
        self.req = urllib.request.Request(self.__url,  headers={'Range': 'bytes=%s' % self.__byteRange})#mendefinisikan modul urllib untuk membuka url

    def getFileData(self):#fungsi getFileData
        return urllib.request.urlopen(self.req).read()#untuk membuka URL, yang bisa berupa string atau objek request


def main(url=None, splitBy=3):#fungsi main
    # menginisialisasikan variable yang mengandung waktu fungsi main dijalankan
    start_time = time.time()#mengembalikan waktu saat ini
    
    # cek apakah variable url mengandung sebuah nilai selain None
    # jika url == None, keluar dari fungsi main
    # jika url != None, lanjutkan fungsi main
    if not url:
        print("Please Enter some url to begin download.")#menampilkan teks tersebut jika url tidak sesuai
        return

    # parsing isi url dengan '/' sebagai patokan split
    fileName = url.split('/')[-1]
    # melakukan request header dari url untuk mendapatkan ukuran file
    sizeInBytes = requests.head(url, headers={'Accept-Encoding': 'identity'}).headers.get('content-length', None)
    #print ukuran file
    print("%s bytes to download." % sizeInBytes)
    
    # cek apakah variable sizeInBytes yang mengandung ukuran file mengandung sebuah nilai selain None
    # jika sizeInBytes == None, keluar dari fungsi main
    # jika sizeInBytes != None, lanjutkan fungsi main
    if not sizeInBytes:
        print("Size cannot be determined.")
        return

    #pendefinisian list kosong (buffer)
    dataLst = []
    # split proses download ke 3 thread (splitBy = 3)
    for idx in range(splitBy):

        # tentukan range byte yang harus di download oleh sebuah thread
        byteRange = buildRange(int(sizeInBytes), splitBy)[idx]
        # menginisialisasikan object SplitBufferThreads dengan url dan range byte yang harus di download
        bufTh = SplitBufferThreads(url, byteRange) 
        #memulai thread
        bufTh.start()
        #menunggu hingga thread selesai 
        bufTh.join() 
        #memasukan semua byte ke buffer (list)
        dataLst.append(bufTh.getFileData())

    content = b''.join(dataLst) #melakukan join bytes dalam buffer (list)

    if dataLst:
        #kondisi jika file sudah ada
        if os.path.exists(fileName): 
            os.remove(fileName) #aksi untuk menghapus file
        
        #print jumlah waktu dari awal hingg akhir
        print("--- %s seconds ---" % str(time.time() - start_time))
        
        #menulis file
        with open(fileName, 'wb') as fh:
            fh.write(content) #melakukan penulisan
        print("Finished Writing file %s" % fileName) #print hasil tulisan

if __name__ == '__main__': #menjalankan program 
    main(url) # memanggil fungsi main dengan url sebagai parameter